class MainController {
  constructor($log) {
    $log.log('Initialized main controller.');
  }
}

export const main = {
  bindings: {
    meals: '='
  },
  controller: MainController,
  controllerAs: 'model',
  template: require('./main.html')
};
