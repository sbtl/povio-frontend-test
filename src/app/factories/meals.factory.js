export const MealsFactory = function ($http) {
  const factory = {
    getMeals
  };

  return factory;

  function getMeals(data) {
    return $http.get('/data/meals.json', data);
  }
};
