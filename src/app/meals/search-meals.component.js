class SearchMealsController {
  constructor($timeout, MealsFactory) {
    this.$timeout = $timeout;
    this.MealsFactory = MealsFactory;

    this.data = {
      key: null,
      place: null,
      radius: null,
      type: null,
      people: null
    };

    // initialize results
    this.getMeals();
  }

  getMeals() {
    this.meals = null;

    // simulating api delay
    this.$timeout(() => {
      this.MealsFactory
        .getMeals(this.data)
        .then(response => {
          this.meals = response.data;
        });
    }, 1000);
  }
}

export const searchMeals = {
  bindings: {
    meals: '='
  },
  controller: SearchMealsController,
  controllerAs: 'model',
  template: require('./search-meals.template.html')
};
