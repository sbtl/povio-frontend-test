class MealsResultsController {
  constructor() {
    this.selectedMeal = null;
    this.smsSent = false;
    this.verificationCompleted = false;
  }

  reserve(id) {
    this.selectedMeal = id;
  }

  sendSms() {
    this.smsSent = true;
  }

  verifyCode() {
    this.verificationCompleted = true;
  }

  close() {
    this.selectedMeal = null;
    this.smsSent = false;
    this.verificationCompleted = false;
  }
}

export const mealsResults = {
  bindings: {
    meals: '='
  },
  controller: MealsResultsController,
  controllerAs: 'model',
  template: require('./meals-results.template.html')
};
