import angular from 'angular';

import 'angular-ui-router';
import routesConfig from './routes';

import {main} from './app/main';
import {footer} from './app/footer';

import {searchMeals} from './app/meals/search-meals.component';
import {mealsResults} from './app/meals/meals-results.component';

import {MealsFactory} from './app/factories/meals.factory';

import './index.scss';

angular
  .module('app', ['ui.router'])
  .config(routesConfig)
  .component('app', main)
  .component('searchMeals', searchMeals)
  .component('mealsResults', mealsResults)
  .component('appFooter', footer)
  .factory('MealsFactory', MealsFactory);
